<?php
/**
 * Created by PhpStorm.
 * User: adouming
 * Date: 15/9/28
 * Time: 上午12:41
 */
function runcmd($cmd) {
    echo $cmd . "\r\n";
    passthru($cmd);
}
function replacefile($search,$replace,$file) {
    $oldcontent = file_get_contents($file);
    $newcontent = str_replace($search,$replace,$oldcontent);
    file_put_contents($file, $newcontent);
}
$url = "url.txt";
$icon = "icon.png";
$default = "default.png";
if(!file_exists($url)) {
    die("put web url to url.txt");
}
if(!file_exists($icon)) {
    die("put icon.png to here");
}
if(!file_exists($default)) {
    die("put default.png to here");
}
echo "正在替换icon\r\n";
$cmd = 'find ./platforms/ios/*/Resources/ -name "icon*.png" -exec identify {} \; | awk \'{print "convert icon.png -resize " $3 " " $1}\' > a.sh && sh a.sh && rm a.sh';
runcmd($cmd);
$cmd = 'find ./platforms/ios/www/ -name "logo.png" -exec identify {} \; | awk \'{print "convert icon.png -resize " $3 " " $1}\' > a.sh && sh a.sh && rm a.sh';
runcmd($cmd);
echo "正在替换splash\r\n";
$cmd = 'find ./platforms/ios/*/Resources/ -name "Default*.png" -exec identify {} \; | awk \'{print "convert default.png -resize " $3 "! " $1}\' > a.sh && sh a.sh && rm a.sh';
runcmd($cmd);
echo "正在替换www资源\r\n";
$cmds = array('find ./www/ -name "icon.png" -exec identify {} \; | awk \'{print "convert icon.png -resize " $3 " " $1}\' > a.sh && sh a.sh && rm a.sh',
    'find ./www/ -name "logo.png" -exec identify {} \; | awk \'{print "convert icon.png -resize " $3 " " $1}\' > a.sh && sh a.sh && rm a.sh',
    'find platforms/ios/www/ -name "logo.png" -exec identify {} \; | awk \'{print "convert icon.png -resize " $3 " " $1}\' > a.sh && sh a.sh && rm a.sh'
);
foreach ($cmds as $cmd) {
    runcmd($cmd);
}
echo "换掉phongap字样\r\n";
$cmd = 'sed -i.bak "s/PhoneGap//g" platforms/ios/www/index.html';
runcmd($cmd);
$cmd = 'sed -i.bak "s/Device is Ready/Wait\.\.\./g" platforms/ios/www/index.html';
runcmd($cmd);
$url = trim(file_get_contents("url.txt"));
$ifile = "./platforms/ios/www/index.html";
$cmd = 'sed -i "s/app\.initialize();/app\.initialize(); window\.location = \'' . $surl .'\';/g" platforms/ios/www/index.html';
$code = 'app.initialize();';
$newcode = 'app.initialize();window.location = \'' . $url .'\';';
replacefile($code,$newcode,$ifile);
$mfile = exec('find ./ -name "MainViewController.m"');
echo "正在处理" . $mfile . "\r\n";
$mvcontent = file_get_contents($mfile);
$code = "// you can do so here.";
$newcode = "// my code is here.\r\n";
$newcode .= "//Lower screen 20px on ios 7\r\n";
$newcode .= "if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {\r\n";
$newcode .= "    CGRect viewBounds = [self.webView bounds];\r\n";
$newcode .= "        viewBounds.origin.y = 20;\r\n";
$newcode .= "        viewBounds.size.height = viewBounds.size.height - 20;\r\n";
$newcode .= "        self.webView.frame = viewBounds;\r\n";
$newcode .= "    }\r\n";

$mvcontent = str_replace($code, $newcode, $mvcontent);

replacefile($code,$newcode,$mfile);
$ifile = exec('find ./ -name "*-Info.plist"');
echo "info" . $ifile . "\r\n";

echo "Adjusting plist for App Transport Security exception.\r\n";
$cmd = '/usr/libexec/plistbuddy -c "add NSAppTransportSecurity:NSAllowsArbitraryLoads bool true" ' . $ifile . ' 2>/dev/null';
runcmd($cmd);
echo "ios  Done\r\n";

$cmds = array('find ./platforms/android/res/ -name "screen.png" -exec identify {} \; | awk \'{print "convert default.png -resize " $3 "! " $1}\' > a.sh && sh a.sh && rm a.sh',
'find ./platforms/android/res/ -name "icon*.png" -exec identify {} \; | awk \'{print "convert icon.png -resize " $3 " " $1}\' > a.sh && sh a.sh && rm a.sh',
'find ./www/ -name "logo.png" -exec identify {} \; | awk \'{print "convert icon.png -resize " $3 " " $1}\' > a.sh && sh a.sh && rm a.sh',
'sed -i.bak "s/PhoneGap//g" platforms/android/assets/www/index.html',
'sed -i.bak "s/Device is Ready/Wait\.\.\./g" platforms/android/assets/www/index.html', 'sed -i "s/app\.initialize();/app\.initialize(); window\.location = \'' . $surl .'\';/g" platforms/android/assets/www/index.html',
);
foreach ($cmds as $cmd) {
    runcmd($cmd);
}
$tfile = "./www/index.html";
$afile = "./platforms/android/assets/www/index.html";
$code = 'app.initialize();';
$newcode = 'app.initialize();' . "\n            " . 'window.location = \'' . $url .'\';';
replacefile($code,$newcode,$tfile);
replacefile($code,$newcode,$afile);


echo "android  Done\r\n";

//get android package name
$AndroidManifest = "platforms/android/AndroidManifest.xml";
$xml_array=simplexml_load_file($AndroidManifest);
$json = json_encode($xml_array);
$manifestArray = json_decode($json, true);
$package_name = $manifestArray["@attributes"]["package"];
//var_dump($manifestArray);
$androidbuildcmd = <<<EOT
rm test-android.keystore
rm -rf platforms/android/gen
rm -rf platforms/android/bin
keytool -genkey -v -alias test-android -keyalg RSA -keysize 2048 -dname 'CN=xxx, OU=xxx, O=xxx, L=xxx, ST=xxx, C=xx' -validity 10000 -keypass password -storepass password -keystore 'test-android.keystore'
phonegap build android
jarsigner  -keystore test-android.keystore -storepass password -signedjar {$package_name}.apk platforms/android/build/outputs/apk/android-release-unsigned.apk test-android
EOT;
$abfilename = "build_android.sh";
file_put_contents($abfilename, $androidbuildcmd);
echo "android build file : $abfilename is ready! \r\n";
